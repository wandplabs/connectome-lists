# ligrec-enzymes

Ligand-receptor lists from the [Connectome](https://github.com/msraredon/Connectome) project, converted into a universal format.

## Description
Cell-cell gene interaction databses typically are built from documented protein-protein interactions. However, a large component of cell-cell communcation is mediated by small molecules which are not associated with gene names. By using the expression of enzymes which catalyze the final steps in the biosynthesis of such molecules to infer their presence, such interactions can be predicted between cell populations from RNA-seq data.

Pairs drawn from Guide2Pharma and DLRP in the original were removed; this list is meant as a supplement to other, more generic ones, and not a replacement.

## Roadmap
- add interactions

## Authors and acknowledgment
The list is a derivitave work of the list published by M.S. Raredon, per the license of the original.

Additional curation by D. Stadtmauer

## License
The list is licensed under the GNU General Public License v3.0 (see LICENSE file)
